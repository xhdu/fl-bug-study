# Understanding the Bug Characteristics and Fix Strategies of Federated Learning Systems

This repository consists of three main folders: Dataset_Init, Manual_Labelling and Quantitative_study.

1. **Dataset_Init** folder: The list of initially mined Issues and Pull Rquests(after Step 1) for each framework from GitHub is presented in the file `GitHub_init/{frameworkname}/issues_init.csv` and `GitHub_init/{frameworkname}/PRs_init.csv`.

    The list of StackOverflow bugs based on tag and keywords is presented in the file [Dataset_init/SO_init/SO_init.csv](Dataset_init/SO_init/SO_init.csv).
2. **Manual_Labelling** folder: In this folder, we have placed all the files associated with our manual labelled result. For the bugs, we further annotate `Symptom`, `Souce of Bug`, `Bug Type` and `Root cause`.

    Noted that bugs in `Documentation` and `Others` are excluded from root cause analysis because of their irrelevance to the core FL functions, and bugs from Pull Requests (PRs) are excluded from `Symptom` because in our observations, most PRs are submitted by framework developers or maintainers with limited information.
3. **Quantitative_Study** folder: In this folder, we have placed the source data from Quantitative Study in Section 7.1. In the lifecycle file from Github, we annotate the creation time, closing time and lifecycle.
In the lifecycle file from StackOverflow, we annotate the creation time, last active time and lifecycle.

    In files of patch size, we annotate the number of added lines, deleted lines, the total number of lines, and changed files.

For the fix strategy, since some instances do not have a clear fix strategy, we summarize some common fix strategies and analyze which root cause can be fixed by them.

**The relationship table between fix strategy and root cause**:  
<div align=center>
    <img src="Relationship_between_fix_and_root_cause.svg" width="600">
</div>
